import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import HomeRoutes from './routers/HomeRoutes';
import Login from './screens/Login';


const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/*" element={<HomeRoutes />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
