import React from 'react'
import { Link, useLocation } from 'react-router-dom';

const Sidebar = () => {

    const location = useLocation();

    const { pathname } = location;

    return (
        <div className="w-75 rounded" >
            <ul className="list-group list-group-flush bg-info" >
                <li className="list-group-item border-0">
                    <Link className={pathname === '/' ? 'active' : ''} style={{ textDecoration: 'none', color: 'black' }} to="/" ><i className="bi bi-house-door-fill m-1"></i>Home</Link>
                </li>
                <li className="list-group-item border-0">
                    <Link className={pathname === '/citas' ? 'active' : ''} style={{ textDecoration: 'none', color: 'black' }} to="/citas" ><i className="m-1 bi bi-calendar-date-fill"></i>Citas</Link>
                </li>
                <li className="list-group-item border-0">
                    <Link className={pathname === '/patients' ? 'active' : ''} style={{ textDecoration: 'none', color: 'black' }} to="/patients" ><i className="bi bi-people-fill m-1"></i>Pacientes</Link>
                </li>
                <li className="list-group-item border-0">
                    <Link className={pathname === '/dentists' ? 'active' : ''} style={{ textDecoration: 'none', color: 'black' }} to="/dentists" > <i className="bi bi-person-fill m-1"></i>Odontologo</Link>
                </li>
                <li className="list-group-item border-0">
                    <Link className={pathname === '/invoices' ? 'active' : ''} style={{ textDecoration: 'none', color: 'black' }} to="/invoices" ><i className="bi bi-receipt m-1"></i>Facturas</Link>
                </li>
                <li className="list-group-item border-0">
                    <Link className={pathname === '/surgeries' ? 'active' : ''} style={{ textDecoration: 'none', color: 'black' }} to="/surgeries" ><i className="m-1 bi bi-bag-plus"></i>Cirugias</Link>
                </li>
                {/* <li className="list-group-item border-0">
                    <Link className={pathname === '/rh' ? 'active' : ''} style={{ textDecoration: 'none', color: 'black' }} to="/surgeries" ><i className="m-1 bi bi-bag-plus"></i>RRHH</Link>
                </li> */}
            </ul>
            {/* <div className="text-center" >
                <img className="img-fluid" width={500} height={500} src="https://www.pngkit.com/png/detail/417-4171952_logo-odontologia-logo-de-odontologia-png.png" alt="logo" />
            </div> */}
        </div>
    )
}

export default Sidebar
