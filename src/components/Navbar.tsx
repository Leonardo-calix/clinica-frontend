import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {
    return (
        <nav className="navbar navbar-dark bg-info">
            <Link to="/" className="navbar-brand text-uppercase"> <i className="m-1 bi bi-calendar3-range"></i> ceesho</Link>
            <div className="form-inline">
                <div className="dropdown">
                    <button className="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                       <img className="img-fluid rounded-circle" width={25} height={25} src="https://w7.pngwing.com/pngs/223/244/png-transparent-computer-icons-avatar-user-profile-avatar-heroes-rectangle-black.png" alt="perfil" />
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <Link className="dropdown-item" to="#">Perfil</Link>
                        <Link className="dropdown-item" to="/login">Logout</Link>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Navbar
