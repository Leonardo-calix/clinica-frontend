import React, { useState } from 'react'

const Patients = () => {

    const [action, setAction] = useState<string>('');

    const data = [
        {
            codigo: 'DENT-1',
            nombre: 'Juan',
            apellido: 'Perez',
            direccion: 'El predregal',
            telefono: '33556789',
        },
        {
            codigo: 'DENT-2',
            nombre: 'Manuel',
            apellido: 'Hernandez',
            direccion: 'San Miguel',
            telefono: '99567895',
        },
        {
            codigo: 'DENT-3',
            nombre: 'Hector',
            apellido: 'Suarez',
            direccion: 'La Sosa',
            telefono: '33556789',
        },
        {
            codigo: 'DENT-4',
            nombre: 'Salvador',
            apellido: 'Contreras',
            direccion: 'El Sauce',
            telefono: '94555589',
        },
        {
            codigo: 'DENT-5',
            nombre: 'Bryan',
            apellido: 'Calix',
            direccion: 'Altos del Trapiche',
            telefono: '34556789',
        },
        {
            codigo: 'DENT-6',
            nombre: 'Kevin',
            apellido: 'Juarez',
            direccion: 'Las Lomas',
            telefono: '33457689',
        },
        {
            codigo: 'DENT-7',
            nombre: 'Yadira',
            apellido: 'Perez',
            direccion: '21 de Octubre',
            telefono: '99807654',
        }
    ]


    return (
        <div>
            <h3 className="text-title p-3" >Listado de pacientes</h3>

            <button onClick={() => setAction('Nuevo')} className="btn btn-sm btn-info mb-3 mt-2" data-toggle="modal" data-target="#staticBackdrop" >Agregar</button>

            <table className="table table-borderless table-sm p-3">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Expediente</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map(item => {
                            return (
                                <tr>
                                    <td> {item.nombre} </td>
                                    <td>{item.apellido} </td>
                                    <td>{item.direccion} </td>
                                    <td>{item.telefono} </td>
                                    <td>
                                        <button type="button" className="btn btn-sm btn-outline-info">
                                           Detalle
                                        </button>
                                    </td>

                                    <td>
                                        <div className="btn-group" role="group" aria-label="Basic example">
                                            <button data-toggle="modal" data-target="#staticBackdrop" onClick={() => setAction('Editar')} type="button" className="btn btn-default text-warning">
                                                <i className="bi bi-pencil-square"></i>
                                            </button>
                                            <button type="button" className="btn btn-default text-danger">
                                                <i className="bi bi-trash2-fill"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            {/* Modal */}
            <div className="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="staticBackdropLabel">{action} Paciente</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form>
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Nombre</label>
                                    <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Nombre completo" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Apellido</label>
                                    <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Apellidos" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Direccion</label>
                                    <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Direccion" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Telefono</label>
                                    <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Telefonos" />
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-sm btn-info">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Patients
