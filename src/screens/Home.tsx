import React from 'react'
import { Link } from 'react-router-dom'

const Home = () => {
    return (
        <div>

            <h3 className="m-2" >Bienvenidos al panel administrativo </h3>

            <div className="row">
                <div className="col-md-4 mt-2 mb-2">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title">Citas</h5>
                            <p className="card-text">La clinica cuenta con la cantidad de 5 citas para el dia de hoy.</p>
                            <Link to="/citas" className="btn btn-sm btn-info btn-md">Detalles</Link>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 mt-2 mb-2">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title">Cirugias</h5>
                            <p className="card-text">La clinica cuenta con la cantidad de 2 cirugias para el dia de hoy.</p>
                            <Link to="/surgeries" className="btn btn-sm btn-info btn-md">Detalles</Link>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 mt-2 mb-2">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title">Pacientes</h5>
                            <p className="card-text">En esta semana la clinica ha registrado 20 pacientes.</p>
                            <Link to="/patients" className="btn btn-sm btn-info btn-md">Detalles</Link>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 mt-2 mb-2">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title">Odontologos</h5>
                            <p className="card-text">La clinica cuenta con 3 odontologos generales, 2 especialistas y 5 odontologos especialistas por llamadas.</p>
                            <Link to="/dentists" className="btn btn-sm btn-info btn-md">Detalles</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home
