import React from 'react'

const Citas = () => {


    const data = [

        {

            nombre: 'Teresa',

            apellido: 'Perez',

            fecha: '10-08-2021',

            hora: '0800',

        },

        {

            nombre: 'Maira',

            apellido: 'Hernandez',

            fecha: '10-08-2021',

            hora: '1000',

        },

        {

            nombre: 'pedro',

            apellido: 'Suarez',

            fecha: '10-08-2021',

            hora: '1500',

        },

        {

            nombre: 'Andrea',

            apellido: 'Espinal',

            fecha: '10-08-2021',

            hora: '0700',

        },

        {

            nombre: 'Alejandro',

            apellido: 'Contreras',

            fecha: '10-08-2021',

            hora: '1000',



        },

        {

            nombre: 'Bryan',

            apellido: 'Calix',

            fecha: '10-08-2021',

            hora: '1100',

        },

        {

            nombre: 'Yadira',

            apellido: 'Canizales',

            fecha: '10-08-2021',

            hora: '0700',



        }

    ]

    return (
        <div>
            <h3 className="text-title p-3" >Listado de citas</h3>

            <button className="btn btn-sm btn-info mb-3 mt-2" >Agregar</button>

            <table className="table table-borderless table-sm p-3">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Hora</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
   
                        data.map(item => {
                            return (
                                <tr>
                                    <td>{item.nombre}</td>
                                    <td> {item.apellido} </td>
                                    <td>{item.fecha} </td>
                                    <td>{item.hora} </td>

                                    <td>
                                        <td><button type="button" className="btn btn-outline-info btn-sm">Detalle</button></td>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Citas
