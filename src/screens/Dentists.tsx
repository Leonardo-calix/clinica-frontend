import React, { useState } from 'react'
import { Link } from 'react-router-dom'

const Dentists = () => {

    const [action, setAction] = useState<string>('')

    const data = [

        {

            codigo: 'ODONT-1',

            nombre: 'Vinicio',

            apellido: 'Espinal',

            cargo: 'Endodoncista',

            direccion: 'Las Lomas',

            telefono: '98581369',

        },

        {

            codigo: 'ODONT -2',

            nombre: 'Cinthia',

            apellido: 'Aguilar',

            cargo: 'Odont. General',

            direccion: 'El Trapiche',

            telefono: '99567895',

        },

        {

            codigo: 'ODONT-3',

            nombre: 'Martha',

            apellido: 'Madrid',

            cargo: 'Implantologa',

            direccion: 'La Sosa',

            telefono: '33556789',

        },

        {

            codigo: 'ODONT-4',

            nombre: 'Diego',

            apellido: 'Contreras',

            cargo: 'Ortodoncista',

            direccion: 'El Sauce',

            telefono: '94555589',

        },

        {

            codigo: 'ODONT-5',

            nombre: 'Karen',

            apellido: 'Calix',

            cargo: 'Odont General',

            direccion: 'Altos del Trapiche',

            telefono: '34556789',

        },

        {

            codigo: 'ODONT-6',

            nombre: 'Andrés',

            apellido: 'Rodríguez',

            cargo: 'cirujano Maxilofacial',

            direccion: 'Las Lomas',

            telefono: '33457689',

        },

        {

            codigo: 'ODONT-7',

            nombre: 'Karla',

            apellido: 'Perez',

            cargo: 'Odont General',

            direccion: '21 de Octubre',

            telefono: '99807654',

        }

    ]




    return (
        <div className="h-75">
            <h3 className="text-title p-3" >Listado de Odontologos</h3>

            <button onClick={() => setAction('Nuevo')} className="btn btn-sm btn-info mb-3 mt-2" data-toggle="modal" data-target="#staticBackdrop" >Agregar</button>

            <table className="table table-borderless table-sm p-3">
                <thead>
                    <tr>
                        <th scope="col">Codigo</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Cargo</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map(item => {
                            return (
                                <tr>
                                    <th scope="row">{item.codigo}</th>
                                    <td> {item.nombre} </td>
                                    <td>{item.apellido} </td>
                                    <td>{item.cargo} </td>
                                    <td>{item.direccion} </td>
                                    <td>{item.telefono} </td>
                                    <td>
                                        <div className="btn-group" role="group" aria-label="Basic example">
                                            <button data-toggle="modal" data-target="#staticBackdrop" onClick={() => setAction('Editar')} type="button" className="btn btn-default text-warning">
                                                <i className="bi bi-pencil-square"></i>
                                            </button>
                                            <button type="button" className="btn btn-default text-danger">
                                                <i className="bi bi-trash2-fill"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            <nav aria-label="Page navigation example" className="text-center d-flex justify-content-center">
                <ul className="pagination">
                    <li className="page-item"><Link className="page-link text-info " to="#">Previous</Link></li>
                    <li className="page-item"><Link className="page-link text-info " to="#">1</Link></li>
                    <li className="page-item"><Link className="page-link text-info " to="#">2</Link></li>
                    <li className="page-item"><Link className="page-link text-info " to="#">3</Link></li>
                    <li className="page-item"><Link className="page-link text-info " to="#">Next</Link></li>
                </ul>
            </nav>

            <div>

                {/* Modal */}
                <div className="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="staticBackdropLabel">{action} Odontologo</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlInput1">Nombre</label>
                                        <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Nombre completo" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlInput1">Apellido</label>
                                        <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Apellidos" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlInput1">Email address</label>
                                        <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Correo" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlInput1">Direccion</label>
                                        <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Direccion" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlInput1">Telefono</label>
                                        <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Telefonos" />
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-sm btn-info">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Dentists
