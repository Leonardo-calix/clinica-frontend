import React from 'react'
import { useNavigate } from 'react-router'

const Login = () => {

  const navigate = useNavigate();


  return (
    <div className="row w-100">
      <div className="col-md-8 col-lg-8 bg-info " style={{ height: '100vh' }} >
        <div className="text-center text-white p-5" >
          <h3 className="m-3 text-title" >Clinica dental CEESHO</h3>
          <p className="text" >Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ut vitae ratione quo temporibus esse maxime earum veritatis iusto dolorem explicabo repellendus, molestias voluptatem commodi fugit sed minus eveniet praesentium est?</p>
          <img src="https://www.kindpng.com/picc/m/55-557906_logo-para-dentistas-png-transparent-png.png" alt="logo" className="img-fluid rounded" />
        </div>
      </div>
      <div className="col-md-4 col-lg-4" style={{ background: 'white', height: '100vh' }}>
        <h3 className="text-center mt-5 mb-2" >Iniciar sesion</h3>
        <div className="p-5">
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Correo electronico</label>
            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese el correo electronico" />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Contraseña</label>
            <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Ingrese la contraseña" />
          </div>
          <div className="form-check">
            <input type="checkbox" className="form-check-input" id="exampleCheck1" />
            <label className="form-check-label" htmlFor="exampleCheck1">Recuerdame</label>
          </div>
          <button type="button" onClick={ () => navigate('/') } className="btn btn-info mt-3 btn-block ">Iniciar session</button>
        </div>
      </div>
    </div>
  )
}

export default Login

