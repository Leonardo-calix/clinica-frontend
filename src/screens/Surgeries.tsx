import React  from 'react'

const Surgeries = () => {

    const data = [

        {

            paciente: 'Teresa Perez',

            odontologo: 'Alejandro Contreras',

            cirugia: 'Tercera Molar',

            hora: '0800',

            estado: 'realizada',

        },

        {

            paciente: 'Maira Hernandez',

            odontologo: 'Alejandro Contreras',

            cirugia: 'Tercera Molar',

            hora: '1000',

            estado: 'realizada',

        },

        {

            paciente: 'Pedro Suarez',

            odontologo: 'Andres Rodriguez',

            cirugia: 'Maxilar inferiro',

            hora: '1500',

            estado: 'pendiente',

        },

        {

            paciente: 'Alejandro Contreras ',

            odontologo: 'Andres Rodriguez',

            cirugia: 'Maxilar inferior',

            hora: '1000',

            estado: 'realizada',

        },

        {

            paciente: 'Bryan Calix ',

            odontologo: 'Andres Rodriguez',

            cirugia: 'Maxilar superior',

            hora: '1100',

            estado: 'realizada',

        },

        {

            paciente: 'Kevin Juarez ',

            odontologo: 'Vinicio Espinal',

            cirugia: 'Frenectomia',

            hora: '1800',

            estado: 'pendiente',

        },

        {

            paciente: 'Yadira Canizales ',

            odontologo: 'Vinicio Espinal',

            cirugia: 'Tercera impactada',

            hora: '0700',

            estado: 'realziada',
        }

    ]

    return (
        <div>
            <h3 className="text-title p-3" >Listado de cirugias</h3>

            <button className="btn btn-sm btn-info mb-3 mt-2" >Agregar</button>

            <table className="table table-borderless table-sm p-3">
                <thead>
                    <tr>
                        <th scope="col">Paciente</th>
                        <th scope="col">Odontologo</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Hora</th>
                        <th scope="col">Estado</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map(item => {
                            return (
                                <tr>
                                    <td>{item.paciente}</td>
                                    <td> {item.odontologo} </td>
                                    <td>{item.cirugia} </td>
                                    <td>{item.hora} </td>
                                    <td>{item.estado} </td>
                                    <td>
                                        <div className="btn-group" role="group" aria-label="Basic example">
                                            <button data-toggle="modal" data-target="#staticBackdrop" type="button" className="btn btn-default text-warning">
                                                <i className="bi bi-pencil-square"></i>
                                            </button>
                                            <button type="button" className="btn btn-default text-danger">
                                                <i className="bi bi-trash2-fill"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Surgeries
