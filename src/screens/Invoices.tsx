import React from 'react'

const Invoices = () => {

    const data = [

        {

            paciente: 'Teresa Pérez',

            fecha: '10-08-2021',

            descripción: 'Cirugía Tercera Molar',

            total: '4500',

        },

        {

            paciente: 'Maira Hernández',

            fecha: '11-08-2021',

            descripción: 'Cirugía Tercera Molar',

            total: '3500',

        },

        {

            paciente: 'Pedro Suarez',

            fecha: '10-08-2021',

            descripción: 'Cirugía maxilar inferior',

            total: '6000',

        },

        {

            paciente: 'Alejandro Contreras ',

            fecha: '12-09-2021',

            descripción: 'Cirugía maxilar inferior',

            total: '6000',

        },

        {

            paciente: 'Bryan Calix ',

            fecha: '15-09-2021',

            descripción: 'Cirugía maxilar superior',

            total: '9000',

        },

        {

            paciente: 'Kevin Juárez ',

            fecha: '16-09-2021',

            descripción: 'Frenectomía',

            total: '2000',

        },

        {

            paciente: 'Yadira Canizales ',

            fecha: '10-10-2021',

            descripción: 'Tercera impactada',

            total: '4000',

        }

    ]

    return (
        <div>
            <h3 className="text-title p-3" >Facturas</h3>

            <button className="btn btn-sm btn-info mb-3 mt-2" >Agregar</button>

            <table className="table table-borderless table-sm p-3">
                <thead>
                    <tr>
                        <th scope="col">Paciente</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">total</th>
                        <th scope="col">Detalle</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map(item => {
                            return (
                                <tr>
                                    <td>{item.paciente}</td>
                                    <td> {item.fecha} </td>
                                    <td>{item.descripción} </td>
                                    <td>{item.total} </td>

                                    <td>
                                        <td><button type="button" className="btn btn-outline-info btn-sm">Detalle</button></td>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Invoices
