import React from 'react'
import { Route, Routes } from 'react-router'
import Navbar from '../components/Navbar'
import Sidebar from '../components/Sidebar'
import Citas from '../screens/Citas'
import Dentists from '../screens/Dentists'
import Home from '../screens/Home'
import Invoices from '../screens/Invoices'
import Patients from '../screens/Patients'
import Surgeries from '../screens/Surgeries'

const HomeRoutes = () => {
    return (
        <div>
            <Navbar />
            <div className="container-fluid bg-img ">
                <div className="row mt-3">
                    <div className="col-md-2 col-lg-2 p-4 m-2 w-100 card border-rounded">
                        <Sidebar />
                    </div>
                    <div className="col-md-9 col-lg-9 w-100" >
                        <div className="container-fluid p-3 m-2 card border-rounded" >
                            <Routes>
                                <Route path="citas" element={<Citas />} />
                                <Route path="new-cita" element={<Citas />} />
                                <Route path="patients" element={<Patients />} />
                                <Route path="dentists" element={<Dentists />} />
                                <Route path="surgeries" element={<Surgeries />} />
                                <Route path="invoices" element={<Invoices />} />
                                <Route path="/" element={<Home />} />
                            </Routes>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default HomeRoutes
